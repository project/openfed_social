<?php

/**
 * @file
 * Contains \Drupal\openfed_social\Form\OpenfedSocialAdminForm.
 */

namespace Drupal\openfed_social\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormBase;

class OpenfedSocialAdminForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'openfed_social_admin_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    // We'll create a multichoice selection table, sortable, with several social
    // networks. A theme selection field will also be added to support a smooth
    // migration from a sharethis solution to Openfed Social.
    $networks = \Drupal::config('openfed_social.settings')
      ->get('openfed_social_networks');
    $networks_enabled = \Drupal::config('openfed_social.settings')
      ->get('openfed_social_networks_enabled');
    $rendering = \Drupal::config('openfed_social.settings')
      ->get('openfed_social_theming');
    $options = $enabled = [];
    $counter = 0;
    $form['weight'] = ['#tree' => TRUE];
    foreach ($networks as $network_key => $network_values) {
      $options[$network_key] = $network_key;
      if (isset($networks_enabled[$network_key])) {
        $enabled[] = $network_key;
      }
      $form['weight'][$network_key] = [
        '#type' => 'weight',
        '#title' => t('Weight for @title', [
          '@title' => $network_values['label'],
        ]),
        '#title_display' => 'invisible',
        '#default_value' => $counter,
        '#attributes' => [
          'class' => [
            'social-networks-order-weight',
          ],
        ],
      ];
      $form['name'][$network_key] = [
        '#markup' => Html::escape($network_values['label']),
      ];
      $form['labels'][$network_key] = [
        '#type' => 'hidden',
        '#value' => $network_values['label'],
      ];
      $counter++;
    }
    $form['enabled'] = [
      '#type' => 'checkboxes',
      '#title' => t('Enabled languages'),
      '#title_display' => 'invisible',
      '#options' => $options,
      '#default_value' => $enabled,
    ];

    $form['redering'] = [
      '#title' => t('Openfed Social SVG rendering type'),
      '#type' => 'radios',
      '#options' => [
        'image' => t('Image tag'),
        'inline' => t('Inline svg'),
      ],
      '#default_value' => $rendering ?: 'image',
    ];
    $moduleHandler = \Drupal::service('module_handler');
    if (!$moduleHandler->moduleExists('openfed_svg_file')) {
      // Rendering choice will be used only if openfed_svg_file is enabled. This
      // module is part of Openfed distribution.
      $form['redering']['#disabled'] = 'true';
      $form['redering']['#default_value'] = 'image';
    }

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => t('Save configuration'),
    ];
    $form['#theme'] = 'openfed_social_admin_form';

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $submissions = $form_state->getValues();
    $existing_networks_settings = \Drupal::config('openfed_social.settings')
      ->get('openfed_social_networks');
    $networks = $networks_enabled = [];
    // Sorting by weight will ensure the right order when saving the variable.
    asort($submissions['weight']);
    foreach ($submissions['weight'] as $network_key => $weight) {
      // We'll add an extra variable so we can easily get the enabled networks.
      if ($submissions['enabled'][$network_key]) {
        $networks_enabled[$network_key] = [
          'label' => $submissions[$network_key],
        ];
      }
      // We've to update the full network list with the current sort order.
      $networks[$network_key] = $existing_networks_settings[$network_key];
    }
    \Drupal::configFactory()
      ->getEditable('openfed_social.settings')
      ->set('openfed_social_networks_enabled', $networks_enabled)
      ->save();
    \Drupal::configFactory()
      ->getEditable('openfed_social.settings')
      ->set('openfed_social_networks', $networks)
      ->save();
    \Drupal::configFactory()
      ->getEditable('openfed_social.settings')
      ->set('openfed_social_theming', $submissions['redering'])
      ->save();
    $this->messenger()->addStatus(t('Configuration saved.'));
    return;
  }
}

