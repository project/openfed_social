<?php

namespace Drupal\openfed_social\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * This will create a Openfed Social Block where social network links will be
 * displayed.
 *
 * @Block(
 *   id = "openfed_social_block",
 *   admin_label = @Translation("Openfed Social Block"),
 *   category = @Translation("Openfed Social Block"),
 * )
 */
class OpenfedSocialBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The path resolver service.
   *
   * @var \Drupal\Core\Extension\ExtensionPathResolver
   */
  protected $pathResolver;

  /**
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param \Drupal\Core\Extension\ExtensionPathResolver $path_resolver
   *   The path resolver service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ExtensionPathResolver $path_resolver) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->pathResolver = $path_resolver;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('extension.path.resolver')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#theme' => 'openfed_social_links_default',
      '#icon_links' => $this->openfed_social_get_icon_links(),
      '#attached' => [
        'library' => ['openfed_social/openfed_social_display_assets'],
      ],
    ];
  }

  /**
   * Generate a list of social icons and links, to be rendered by Openfed Social
   * Block.
   *
   * @return array of icons with links.
   */
  private function openfed_social_get_icon_links() {
    // Use current page' url and title to complete each share link information.
    $url = Url::fromRoute('<current>', [], ['absolute' => 'true']);
    // Use site name as default title.
    $title = \Drupal::config('system.site')->get('name');
    $route_object = \Drupal::routeMatch()->getRouteObject();

    // Grabs the title from the current route.
    if ($route_object !== NULL) {
      $title = \Drupal::service('title_resolver')
        ->getTitle(\Drupal::request(), $route_object);
    }

    $title = is_array($title) ? $title['#markup'] : $title;

    // Getting all the enabled social networks, rendering them based on the user
    // defined option.
    $networks_enabled = \Drupal::config('openfed_social.settings')
      ->get('openfed_social_networks_enabled');
    if ($networks_enabled) {
      return $this->openfed_social_render_sharelinks($url, $title, $networks_enabled);
    }
    return [];
  }

  /**
   * Return an array of social networks share links, completed with the URL and/or
   * Title of the page.
   *
   * @param Url $url
   * @param string $title
   * @param array $networks_enabled
   *
   * @return array of enabled networks with corresponding share links.
   */
  function openfed_social_render_sharelinks($url, $title, $networks_enabled) {
    $path = $this->pathResolver->getPath('module', 'openfed_social');
    $networks = \Drupal::config('openfed_social.settings')
      ->get('openfed_social_networks');
    $rendering = \Drupal::config('openfed_social.settings')
      ->get('openfed_social_theming');
    $sharelinks = [];
    // Creating 2 arrays, one with simple links and one with link and icon. Simple
    // links will be used for ShareThis theme support, icons and links will be
    // used on default theme.
    foreach ($networks_enabled as $network_key => $network) {

      $width = $height= 30;
      $alt = $networks[$network_key]['share_label'];
      $class = 'openfed_social_buttons';

      if ($rendering == 'inline') {
        $attributes['width'] = $width;
        $attributes['height'] = $height;
        $attributes['alt'] = $alt;
        $attributes['title'] = '';

        $uri = $path . '/assets/images/' . $network_key . '.svg';

        // We need the following for inline svgs.
        $svg_data = NULL;
        $svg_file = file_exists($uri) ? file_get_contents($uri) : NULL;
        if ($svg_file) {
          $dom = new \DomDocument();
          libxml_use_internal_errors(TRUE);
          $dom->loadXML($svg_file);
          if (isset($dom->documentElement)) {
            $dom->documentElement->setAttribute('height', $attributes['height']);
            $dom->documentElement->setAttribute('width', $attributes['width']);
          }
          $svg_data = $dom->C14N();
        }

        $icon = [
          '#theme' => 'openfed_svg_file__' . $rendering,
          '#attributes' => $attributes,
          '#alt_text' => NULL,
          '#svg_data' => $svg_data,
          '#uri' => $uri,
        ];
      } else {
        $icon = [
          '#theme' => 'image',
          '#uri' => $path . '/assets/images/' . $network_key . '.svg',
          '#alt' => $alt,
          '#width' => $width,
          '#height' => $height,
          '#attributes' => ['class' => $class],
        ];
      }

      // Special case for email. We should add a target attribute.
      $target = ($network_key == 'email') ? [] : ['target' => '_blank'];
      // Special case for print. URL won't be valid so'll add a tag.
      $fragment = [];
      if ($network_key == 'print') {
        $networks[$network_key]['url'] = $url->toString(); // The current page.
        $fragment = ['fragment' => 'print'];
      }
      // Define link attributes and create a share link.
      $link_attributes = [
        'attributes' => [
          'class' => [
            'openfed_social_share_link',
            'openfed_social_share_link_' . $network_key,
          ],
        ],
      ];
      $link_attributes['attributes'] += $target;
      $share_link = Url::fromUri(str_replace('@title', $title, str_replace('@url', $url->toString(), $networks[$network_key]['url'])), $link_attributes + $fragment);

      $sharelinks[$network_key] = [
        '#type' => 'link',
        '#title' => $icon,
        '#url' => $share_link,
      ];
    }
    return $sharelinks;
  }
}
