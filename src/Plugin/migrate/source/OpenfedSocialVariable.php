<?php

namespace Drupal\openfed_social\Plugin\migrate\source;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\Variable;

/**
 * Preprocess variables from the source database.
 *
 * @MigrateSource(
 *   id = "openfed_social_variable",
 *   source_module = "ofed_social",
 * )
 */
class OpenfedSocialVariable extends Variable {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, StateInterface $state, EntityTypeManagerInterface $entity_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration, $state, $entity_manager);
    $this->variables = $this->configuration['variables'];
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $config = \Drupal::config('openfed_social.settings');
    $openfed_social_networks_d8 = $config->get('openfed_social_networks');

    // We just need to keep the networks order and enabled networks.
    // So just import that but keep the D8 module's config inside items.

    // Clean up ofed_social_networks variable.
    $ofed_social_networks_d7 = $row->getSourceProperty('ofed_social_networks');
    foreach ($ofed_social_networks_d7 as $network_id => $value) {
      $openfed_social_networks_d7[$network_id] = $openfed_social_networks_d8[$network_id];
    }
    $row->setSourceProperty('ofed_social_networks', $openfed_social_networks_d7);

    // Clean up ofed_social_networks_enabled variable.
    $ofed_social_networks_enabled_d7 = $row->getSourceProperty('ofed_social_networks_enabled');
    foreach ($ofed_social_networks_enabled_d7 as $network_id => $value) {
      $ofed_social_networks_enabled_d7[$network_id]['label'] = $openfed_social_networks_d8[$network_id]['label'];
    }
    $row->setSourceProperty('ofed_social_networks_enabled', $ofed_social_networks_enabled_d7);

    return parent::prepareRow($row);
  }

}
